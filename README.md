# Las Plantas Sagradas Y La Rosa Negra - Composición para oferenda n.25

Las Plantas Sagradas Y La Rosa Negra no Soundcloud:
* https://soundcloud.com/ingr_d/sets/las-plantas-sagradas-y-la-rosa

Este repositório contém o código-fonte TidalCycles, SuperCollider e samples utilizados
para produzir a música Composición para oferenda n.25 (feat INGRID) do álbum colaborativo
Las Plantas Sagradas Y La Rosa Negra.

Composición para oferenda n.25 (feat INGRID) no SoundCloud:
* https://soundcloud.com/joenio/composicion-para-oferenda-n25

Mais em: http://joenio.me/las-plantas-sagradas
